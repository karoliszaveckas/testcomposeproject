package com.example.testcomposeproject.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.testcomposeproject.ui.components.circlegauges.ConsumptionCircleGauge
import com.example.testcomposeproject.ui.theme.TestComposeProjectTheme
import com.example.testcomposeproject.ui.theme.WattsOnTheme
import kotlin.math.roundToInt
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestComposeProjectTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = WattsOnTheme.colors.primary_white
                ) {
                    Column(
                        Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState()),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        var randomPercentage by remember {
                            mutableStateOf(Random.nextFloat())
                        }
                        Spacer(modifier = Modifier.height(100.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(
                                text = "${(randomPercentage * 100).roundToInt()} %",
                                modifier = Modifier.padding(horizontal = 8.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(16.dp))
                        Slider(
                            value = randomPercentage * 100,
                            onValueChange = { randomPercentage = it / 100 },
                            valueRange = 0f..250f,
                            modifier = Modifier.padding(horizontal = 24.dp)
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                            Button(onClick = { randomPercentage -= 0.1f }) { Text(text = "-10") }
                            Button(onClick = { randomPercentage -= 0.05f }) { Text(text = "-5") }
                            Button(onClick = { randomPercentage -= 0.01f }) { Text(text = "-1") }
                            Button(onClick = { randomPercentage += 0.01f }) { Text(text = "+1") }
                            Button(onClick = { randomPercentage += 0.05f }) { Text(text = "+5") }
                            Button(onClick = { randomPercentage += 0.1f }) { Text(text = "+10") }
                        }
                        Spacer(modifier = Modifier.height(40.dp))
                        ConsumptionCircleGauge(
                            percentage = randomPercentage,
                            modifier = Modifier.size(280.dp),
                        )
                        Spacer(modifier = Modifier.height(40.dp))

                    }
                }
            }
        }
    }
}