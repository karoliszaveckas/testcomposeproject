package com.example.testcomposeproject.ui.components.circlegauges

import androidx.annotation.DrawableRes
import androidx.annotation.FloatRange
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.testcomposeproject.R
import com.example.testcomposeproject.ui.theme.TestComposeProjectTheme
import com.example.testcomposeproject.ui.theme.WattsOnTheme

/**
 * A circle gauge for displaying consumption against budget. Refer to [BaseCircleGauge] for
 * description about parameters and usage.
 */
@Composable
fun ConsumptionCircleGauge(
    @FloatRange(0.0, Double.POSITIVE_INFINITY)
    percentage: Float,
    modifier: Modifier = Modifier,
    gaugeBaseColor: Color = ConsumptionsCircleGaugeTokens.gaugeBaseColor,
    colorsFirstArc: List<Color> = ConsumptionsCircleGaugeTokens.colorsGreenToYellow,
    colorsSecondArc: List<Color> = ConsumptionsCircleGaugeTokens.colorsYellowToRed,
    animationDurationInMillis: Int = ConsumptionsCircleGaugeTokens.ANIMATION_DURATION,
    mainValueTextColor: Color = ConsumptionsCircleGaugeTokens.mainValueTextColor,
    mainValueTextStyle: TextStyle = ConsumptionsCircleGaugeTokens.mainValueTextStyle,
    percentSignColor: Color = ConsumptionsCircleGaugeTokens.percentSignColor,
    percentSignTextStyle: TextStyle = ConsumptionsCircleGaugeTokens.percentSignTextStyle,
    budgetStatusIconColor: Color = ConsumptionsCircleGaugeTokens.budgetStatusIconColor,
    @DrawableRes powerIconResourceId: Int = ConsumptionsCircleGaugeTokens.powerIconResourceId,
    powerIconColor: Color = ConsumptionsCircleGaugeTokens.powerIconColor,
) {
    BaseCircleGauge(
        percentage = percentage,
        gaugeBaseColor = gaugeBaseColor,
        colorsFirstArc = colorsFirstArc,
        colorsSecondArc = colorsSecondArc,
        animationDurationInMillis = animationDurationInMillis,
        mainValueTextColor = mainValueTextColor,
        mainValueTextStyle = mainValueTextStyle,
        percentSignColor = percentSignColor,
        percentSignTextStyle = percentSignTextStyle,
        budgetStatusIconColor = budgetStatusIconColor,
        topIconResourceId = powerIconResourceId,
        topIconColor = powerIconColor,
        modifier = modifier,
    )
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
private fun ConsumptionCircleGaugePreview() {
    TestComposeProjectTheme {
        Column(
            modifier = Modifier.padding(horizontal = 40.dp),
            verticalArrangement = Arrangement.Center
        ) {
            ConsumptionCircleGauge(percentage = 0.8f)
        }
    }
}

private object ConsumptionsCircleGaugeTokens {

    const val ANIMATION_DURATION = 500
    val gaugeBaseColor: Color @Composable get() = WattsOnTheme.colors.primary_grey_02

    val colorsGreenToYellow: List<Color> = listOf(
        Color(0xFF4CA636),
        Color(0xFF71BE41),
        Color(0xFF92D340),
        Color(0xFFF0D800),
    )
    val colorsYellowToRed: List<Color> = listOf(
        // TODO: Copied the last color from green-yellow list, check with Martin if that's ok.
        Color(0xFFF0D800),
        Color(0xFFFFA800),
        Color(0xFFE40000),
    )

    val mainValueTextColor: Color @Composable get() = WattsOnTheme.colors.primary_black_off
    // TODO: Temporary just using the any font while we don't have the actual fonts
    val mainValueTextStyle: TextStyle
        @Composable get() = MaterialTheme.typography.displayLarge.copy(
            fontSize = 82.sp,
            lineHeight = 82.sp,
        )

    val percentSignColor: Color @Composable get() = WattsOnTheme.colors.primary_grey_04
    val percentSignTextStyle: TextStyle @Composable get() = MaterialTheme.typography.headlineSmall
    val budgetStatusIconColor: Color @Composable get() = WattsOnTheme.colors.primary_black_off
    @DrawableRes val powerIconResourceId: Int = R.drawable.icon_power
    val powerIconColor: Color @Composable get() = WattsOnTheme.colors.primary_grey_05
}