package com.example.testcomposeproject.ui.components.circlegauges

import androidx.annotation.DrawableRes
import androidx.annotation.FloatRange
import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.Easing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.testcomposeproject.R
import com.example.testcomposeproject.ui.theme.TestComposeProjectTheme
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

/**
 * A circle gauge that animates value changes. The gauge can draw max 2 full circles; values above
 * 2f will not be drawn but text in the middle will change.
 * @param percentage any Float value from 0f to Infinity, where 1f represents 100%.
 * @param gaugeBaseColor the color of the arc that indicates the base of the gauge.
 * @param colorsFirstArc the list of colors that will be used to create a gradient for the first
 * arc. Last color must match the first color of [colorsSecondArc] for smooth transition. Minimum
 * two colors required. If only 1 passed, it will be used as both starting and ending color. If 0
 * passed - [Color.Transparent] gradient will be applied.
 * @param colorsSecondArc the list of colors that will be used to create a gradient for the second
 * arc. First color must match the last color of [colorsFirstArc] for smooth transition. Minimum
 * two colors required. If only 1 passed, it will be used as both starting and ending color. If 0
 * passed, then [Color.Transparent] gradient will be applied.
 * @param animationDurationInMillis the duration of the entire animation when [percentage] changes.
 */
@Composable
fun BaseCircleGauge(
    @FloatRange(0.0, Double.POSITIVE_INFINITY)
    percentage: Float,
    gaugeBaseColor: Color,
    colorsFirstArc: List<Color>,
    colorsSecondArc: List<Color>,
    animationDurationInMillis: Int,
    mainValueTextColor: Color,
    mainValueTextStyle: TextStyle,
    percentSignColor: Color,
    percentSignTextStyle: TextStyle,
    budgetStatusIconColor: Color,
    @DrawableRes topIconResourceId: Int,
    topIconColor: Color,
    modifier: Modifier = Modifier,
) {
    Box(
        modifier = modifier.aspectRatio(1f),
    ) {
        CircleGaugeDrawing(
            percentFilled = percentage.coerceAtLeast(0f),
            gaugeBaseColor = gaugeBaseColor,
            colorsFirstArc = colorsFirstArc,
            colorsSecondArc = colorsSecondArc,
            animationDurationInMillis = animationDurationInMillis,
        )

        Column(
            modifier = modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            /* TODO: Figure out with Martin alignment of Icon next to text. Should be done when
            *   fonts are finalized and the font height is known as there doesn't seem to be a
            *   solution right now to align with the top of the font.  */
            Row {
                BudgetStatusIcon(
                    trend = if (percentage >= 1f) {
                        BudgetStatus.AboveBudget
                    } else {
                        BudgetStatus.BelowBudget
                    },
                    iconColor = budgetStatusIconColor,
                    modifier = Modifier.align(Alignment.Top)
                )
                Text(
                    text = (percentage * 100f).roundToInt().toString(),
                    style = mainValueTextStyle,
                    color = mainValueTextColor,
                    modifier = Modifier
                        .wrapContentSize()
                        .alignByBaseline()
                )
                Text(
                    text = "%",
                    style = percentSignTextStyle,
                    color = percentSignColor,
                    modifier = Modifier.alignByBaseline()
                )
            }
        }

        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top,
        ) {
            Spacer(modifier = Modifier.weight(0.14f))
            Box(modifier = Modifier.weight(0.86f)){
                Icon(
                    painter = painterResource(id = topIconResourceId),
                    contentDescription = null, // Decorative
                    tint = topIconColor,
                    modifier = Modifier
                        .size(CircleGaugeTokens.topIconSize)
                        .align(Alignment.TopCenter)
                )
            }
        }
    }
}

@Composable
fun CircleGaugeDrawing(
    @FloatRange(0.0, Double.POSITIVE_INFINITY)
    percentFilled: Float,
    gaugeBaseColor : Color,
    colorsFirstArc: List<Color>,
    colorsSecondArc: List<Color>,
    animationDurationInMillis: Int,
    modifier : Modifier = Modifier,
) {

    var previousPercentFilled by remember { mutableStateOf(0f) }
    var gaugeValues by remember {
        mutableStateOf(
            GaugeValues(
                isGoingUp = true,
                percentFilled = 0f,
                animationDuration = AnimationDuration(0,0,0,0)
            )
        )
    }

    val firstArcTransition =
        updateTransition(targetState = gaugeValues, label = "Color transition")
    val secondArcTransition =
        updateTransition(targetState = gaugeValues, label = "Second Sweep Transition")
    val endCapTransition =
        updateTransition(targetState = gaugeValues, label = "End Cap transition")

    /* This is an animated float value that animates change between previous and current percent
    value. This dynamic value is required where there is a need to calculate new colors on the fly
    as the transition from one percent value to another is ongoing. */
    val percentFilledAnimatedState by animateFloatAsState(
        targetValue = gaugeValues.percentFilled,
        animationSpec = defaultCircleAnimation(durationMillis = animationDurationInMillis)
    )

    LaunchedEffect(key1 = percentFilled) {
        gaugeValues = GaugeValues(
            isGoingUp = (percentFilled - previousPercentFilled) > 0,
            percentFilled = percentFilled,
            animationDuration = getAnimationDuration(
                previousPercentFilled,
                percentFilled,
                animationDurationInMillis
            )
        )
        previousPercentFilled = percentFilled
    }

    val colorStopsFirstArc = rememberSortedColorStops(colors = colorsFirstArc)
    /* This adjustment is needed due to a weird bug where the ending color is slightly visible
    between start cap and start of the first arc. This adjusted color stops array adds the first
    color to the very end of the color stops array so that the color that tears through is the same
    as the starting color of the gradient and then no problem appears. */
    val colorStopsFirstArcAdjustedPairArray =
        rememberAdjustedColorStopsPairArray(colorStops = colorStopsFirstArc)

    val colorStopsSecondArc = rememberSortedColorStops(colors = colorsSecondArc)
    /* This adjustment is needed due to a weird bug where the ending color is slightly visible
    between start cap and start of the first arc. This adjusted color stops array adds the first
    color to the very end of the color stops array so that the color that tears through is the same
    as the starting color of the gradient and then no problem appears. */
    val colorStopsSecondArcAdjustedPairArray =
        rememberAdjustedColorStopsPairArray(colorStops = colorStopsSecondArc)

    /* This is meant to make the start cap appear immediately when a value is set to something else
    than 0f. But if the percent value is set to 0f, we want to make the cap disappear only when the
    arcs' animation has finished */
    val startCapColor by firstArcTransition.animateColor(
        label = "Animate Start Cap Color",
        transitionSpec = {
            when (percentFilled) {
                0f -> defaultCircleAnimation(
                    durationMillis = 25,
                    delayMillis = animationDurationInMillis
                )
                else -> defaultCircleAnimation(durationMillis = 0)
            }
        }
    ) { mGaugeValues ->
        when (mGaugeValues.percentFilled) {
            0f -> Color.Transparent
            else -> colorStopsFirstArc[0].color
        }
    }

    val firstArcSweepAngle by firstArcTransition.animateFloat(
        label = "Animate First Value Arc",
        transitionSpec = {
            defaultCircleAnimation(
                durationMillis = targetState.animationDuration.firstArcDuration,
                delayMillis = targetState.animationDuration.firstArcDelay
            )
        }
    ) { mGaugeValues ->
        // Stop drawing the arc if reached full circle
        if (mGaugeValues.percentFilled > 1f) 360f else mGaugeValues.percentFilled * 360f
    }

    val secondArcSweepAngle by secondArcTransition.animateFloat(
        label = "Animate Second Value Arc",
        transitionSpec = {
            defaultCircleAnimation(
                durationMillis = targetState.animationDuration.secondArcDuration,
                delayMillis = targetState.animationDuration.secondArcDelay
            )
        }
    ) { mGaugeValues ->
        when {
            mGaugeValues.percentFilled > 1f && mGaugeValues.percentFilled < 2f ->
                mGaugeValues.percentFilled % 1f * 360f
            /* We want to stop drawing the second arc once it reached full circle */
            mGaugeValues.percentFilled >= 2f -> 360f
            else -> 0f
        }
    }

    val endCapRotation by endCapTransition.animateFloat(
        label = "Animate End Cap Rotation",
        transitionSpec = { defaultCircleAnimation(durationMillis = animationDurationInMillis) }
    ) { mGaugeValues ->
        // Max value can be 2 full circles - 90f for starting at 12 o'clock
        (mGaugeValues.percentFilled * 360f - 90f).coerceAtMost(360 * 2f - 90f)
    }

    val endCapColor by remember(percentFilledAnimatedState) {
        mutableStateOf(
            when {
                percentFilledAnimatedState == 0f -> Color.Transparent
                percentFilledAnimatedState > 0f && percentFilledAnimatedState <= 1f ->
                    getEndCapColor(percentFilledAnimatedState, colorStopsFirstArc)
                else -> getEndCapColor(
                    (percentFilledAnimatedState - 1f).coerceIn(0f, 1f),
                    colorStopsSecondArc
                )
            }
        )
    }

    val endCapBrush by remember(percentFilledAnimatedState) {
        mutableStateOf(
            Brush.sweepGradient(
                colorStops = listOf(
                    0f to endCapColor,
                    0.9f to Color.Transparent,
                    /* Super tiny gap can appear for some reason before the end cap, so to avoid
                    that - we are setting the last bit of the gradient to once again have the
                    starting color, so they match and the gap is not visible. */
                    0.91f to endCapColor,
                ).toTypedArray()
            )
        )
    }

    /* This is needed because of a strange bug where the first arc's color is just slightly
    showing before the start of second arc even though the second arc should be on top of it. With
    this we are changing the main arc's colorStops to include the last stop's color as the first
    stop. */
    val firstArcColorStops by remember {
        derivedStateOf {
            when {
                percentFilledAnimatedState >= 1f -> colorStopsFirstArc.toPairArray()
                else -> colorStopsFirstArcAdjustedPairArray
            }
        }
    }

    Canvas(modifier = modifier.fillMaxSize()) {
        val strokeWidth = size.minDimension * CircleGaugeTokens.STROKE_WIDTH_RATIO
        val size = Size(width = size.width - strokeWidth, height = size.height - strokeWidth)

        rotate(-90f) {
            // BASE ARC
            drawArc(
                startAngle = 0f,
                sweepAngle = 360f,
                style = Stroke(width = strokeWidth, cap = StrokeCap.Butt, join = StrokeJoin.Round),
                useCenter = false,
                color = gaugeBaseColor,
                topLeft = Offset(x = strokeWidth / 2, y = strokeWidth / 2),
                size = size
            )
            // START CAP
            drawArc(
                startAngle = 0f,
                sweepAngle = 0.1f,
                style = Stroke(width = strokeWidth, cap = StrokeCap.Round),
                useCenter = false,
                color = startCapColor,
                topLeft = Offset(x = strokeWidth / 2, y = strokeWidth / 2),
                size = size,
            )
            // MAIN VALUE ARC
            drawArc(
                startAngle = 0f,
                sweepAngle = firstArcSweepAngle,
                style = Stroke(width = strokeWidth, cap = StrokeCap.Butt),
                useCenter = false,
                brush = Brush.sweepGradient(colorStops = firstArcColorStops),
                topLeft = Offset(x = strokeWidth / 2, y = strokeWidth / 2),
                size = size,
            )
            // SECOND VALUE ARC (when percent > 100)
            drawArc(
                startAngle = 0f,
                sweepAngle = secondArcSweepAngle,
                style = Stroke(width = strokeWidth, cap = StrokeCap.Butt),
                useCenter = false,
                brush = Brush.sweepGradient(colorStops = colorStopsSecondArcAdjustedPairArray),
                topLeft = Offset(x = strokeWidth / 2, y = strokeWidth / 2),
                size = size,
            )
        }
        rotate(endCapRotation) {
            // END CAP
            drawArc(
                startAngle = 0f,
                sweepAngle = 0.1f,
                style = Stroke(width = strokeWidth, cap = StrokeCap.Round, join = StrokeJoin.Round),
                useCenter = false,
                brush = endCapBrush,
                topLeft = Offset(x = strokeWidth / 2, y = strokeWidth / 2),
                size = size
            )
        }
    }
}

@Composable
fun BudgetStatusIcon(
    trend: BudgetStatus,
    iconColor: Color,
    modifier: Modifier,
) {
    Crossfade(targetState = trend, modifier = modifier) { trending ->
        when (trending) {
            BudgetStatus.AboveBudget -> Icon(
                painter = painterResource(id = R.drawable.tendency_up_small),
                contentDescription = null, // Decorative
                tint = iconColor,
                modifier = Modifier.size(CircleGaugeTokens.budgetStatusIconSize)
            )
            BudgetStatus.BelowBudget -> Icon(
                painter = painterResource(id = R.drawable.tendency_down_small),
                contentDescription = null, // Decorative
                tint = iconColor,
                modifier = Modifier.size(CircleGaugeTokens.budgetStatusIconSize)
            )
        }
    }
}

private fun getAnimationDuration(
    previousPercentFilled: Float,
    newPercentFilled: Float,
    animationDurationInMillis: Int,
): AnimationDuration {
    val absoluteChangeWithin0To1 =
        getChangeWithin0To1(previousPercentFilled, newPercentFilled).absoluteValue
    val absoluteTotalChangeAbove1 =
        getChangeWithin1To2(previousPercentFilled, newPercentFilled).absoluteValue +
                getChangeFrom2Upwards(previousPercentFilled, newPercentFilled).absoluteValue
    val totalAbsoluteChange = (newPercentFilled - previousPercentFilled).absoluteValue

    val isGoingUp = (newPercentFilled - previousPercentFilled) > 0

    val firstArcDuration =
        (animationDurationInMillis * absoluteChangeWithin0To1 / totalAbsoluteChange).roundToInt()
    val secondArcDuration =
        (animationDurationInMillis * absoluteTotalChangeAbove1 / totalAbsoluteChange).roundToInt()

    return AnimationDuration(
        firstArcDuration = firstArcDuration,
        firstArcDelay = if (isGoingUp && firstArcDuration > 0) 0 else secondArcDuration,
        secondArcDuration = secondArcDuration,
        secondArcDelay = if (isGoingUp && secondArcDuration > 0) firstArcDuration else 0
    )
}

private fun getChangeWithin0To1(previousValue: Float, newValue: Float): Float {
    val previousMax = previousValue.coerceAtMost(1f)
    val currentWithin0To100 = newValue.coerceIn(0f, 1f)
    return currentWithin0To100 - previousMax
}

private fun getChangeWithin1To2(previousValue: Float, newValue: Float): Float {
    val previousWithinRange = previousValue.coerceIn(1f, 2f)
    val newWithinRange = newValue.coerceIn(1f, 2f)
    return newWithinRange - previousWithinRange
}

private fun getChangeFrom2Upwards(previousValue: Float, newValue: Float): Float {
    val previousAbove2 = previousValue.coerceAtLeast(2f)
    val newAbove2 = newValue.coerceAtLeast(2f)
    return newAbove2 - previousAbove2
}

private fun getEndCapColor(percentValue: Float, colorStops: List<ColorStop>): Color {
    val surroundingStops = findSurroundingColorStops(percentValue, colorStops)
    return surroundingStops?.let { stops ->
        val endingStopValue =
            if (stops.second.position < stops.first.position) stops.second.position + 1f else stops.second.position
        val percentBetweenPoints =
            (percentValue - stops.first.position) / (endingStopValue - stops.first.position)
        val color = pointBetweenColors(stops.first.color, stops.second.color, percentBetweenPoints)
        color
    } ?: Color.Transparent
}

/**
 * Returns a Pair or [ColorStop]s that are closest to the given [percentValue]. If the percent
 * value matches the first or the last [ColorStop.position] then that [ColorStop] will be taken as
 * a 'surrounding' [ColorStop].
 * @param percentValue from 0f to any positive [Float] value.
 * @param sortedColorStops List of [ColorStop]s from which to find the surrounding [ColorStop] for
 * given [percentValue]
 */
private fun findSurroundingColorStops(
    @FloatRange(0.0, Double.POSITIVE_INFINITY) percentValue: Float,
    sortedColorStops: List<ColorStop>
): Pair<ColorStop, ColorStop>? {

    if (percentValue < 0f) {
        return null
    }

    /* First, check if we find a value equal to one of the stops and if so, return surrounding
    values */
    val foundEqualValue = sortedColorStops.find { it.position == percentValue }
    if (foundEqualValue != null) {
        val indexOfEqualValue = sortedColorStops.indexOf(foundEqualValue)
        return Pair(
            sortedColorStops.getItemBeforeIndex(indexOfEqualValue),
            sortedColorStops.getItemAfterIndex(indexOfEqualValue)
        )
    }
    /* If no equal value, then find surrounding values */
    for (i in 0 until sortedColorStops.size - 1) {
        if (percentValue > sortedColorStops[i].position && percentValue < sortedColorStops[i + 1].position) {
            return Pair(sortedColorStops[i], sortedColorStops[i + 1])
        }
    }

    return null
}

private fun List<ColorStop>.getItemBeforeIndex(index: Int): ColorStop {
    return if (index - 1 < 0) this[this.lastIndex] else this[index - 1]
}

private fun List<ColorStop>.getItemAfterIndex(index: Int): ColorStop {
    return if (index + 1 > this.lastIndex) this.last() else this[index + 1]
}

private fun List<ColorStop>.toPairArray(): Array<Pair<Float, Color>> {
    return this.map { it.toPair() }.toTypedArray()
}

private fun pointBetweenSingleRGBColorValues(from: Float, to: Float, percent: Float): Float =
    from + percent * (to - from)

private fun pointBetweenColors(from: Color, to: Color, percent: Float) =
    Color(
        red = pointBetweenSingleRGBColorValues(from.red, to.red, percent),
        green = pointBetweenSingleRGBColorValues(from.green, to.green, percent),
        blue = pointBetweenSingleRGBColorValues(from.blue, to.blue, percent),
        alpha = pointBetweenSingleRGBColorValues(from.alpha, to.alpha, percent),
    )

/**
 * Distributes a list of [Color]s in equal intervals from 0f to 1f, creates and returns a
 * List of [ColorStop]s
 */
private fun List<Color>.toColorStops(): List<ColorStop> {
    val stopSize = 1f / (this.size - 1)
    val colorStops = mutableListOf<ColorStop>()
    var stop = 0f
    this.forEachIndexed { index, color ->
        if (index == this.lastIndex) {
            stop = 1f // Ensuring last stop is at 1f in case of rounding issues
        }
        colorStops.add(ColorStop(stop, color))
        stop += stopSize
    }
    return colorStops
}

@Composable
fun rememberSortedColorStops(colors: List<Color>): List<ColorStop> {
    return remember(colors) {
        // Minimum two colors are needed for the gradient to not throw an exception.
        when (colors.size) {
            0 -> listOf(Color.Transparent, Color.Transparent)
            1 -> listOf(colors[0], colors[0])
            else -> colors
        }.toColorStops().sortedBy { it.position }
    }
}

@Composable
fun rememberAdjustedColorStopsPairArray(colorStops: List<ColorStop>): Array<Pair<Float, Color>> {
    return remember(colorStops) {
        val lastColorStopAdjusted = colorStops.last().copy(position = 0.99f)
        val newLastColorStop = colorStops.first().copy(position = 1f)
        colorStops
            .toMutableList()
            .minus(colorStops.last())
            .plus(listOf(lastColorStopAdjusted, newLastColorStop))
            .sortedBy { it.position }
            .toPairArray()
    }
}

private fun <T> defaultCircleAnimation(
    durationMillis: Int,
    delayMillis: Int = 0,
    easing: Easing = LinearEasing
): TweenSpec<T> = TweenSpec(durationMillis, delayMillis, easing)

data class ColorStop(
    @FloatRange(0.0, 1.0)
    val position: Float,
    val color: Color
) {
    fun toPair(): Pair<Float, Color> = Pair(position, color)
}

enum class BudgetStatus {
    AboveBudget,
    BelowBudget,
}

data class GaugeValues(
    val isGoingUp: Boolean,
    val percentFilled: Float,
    val animationDuration: AnimationDuration,
)

/** Holds values for animation durations and delays in milliseconds. */
data class AnimationDuration(
    val firstArcDuration: Int,
    val firstArcDelay: Int,
    val secondArcDuration: Int,
    val secondArcDelay: Int,
)

private object CircleGaugeTokens {

    const val STROKE_WIDTH_RATIO: Float = 0.0857f
    val budgetStatusIconSize: DpSize = DpSize(20.6.dp, 14.71.dp)
    val topIconSize: Dp = 42.dp

}

@Preview(showBackground = true)
@Composable
private fun BaseCircleGaugePreview() {
    TestComposeProjectTheme {
        BaseCircleGauge(
            percentage = 0.9f,
            gaugeBaseColor = Color.LightGray,
            colorsFirstArc = listOf(Color.Green, Color.Yellow),
            colorsSecondArc = listOf(Color.Yellow, Color.Red),
            animationDurationInMillis = 500,
            mainValueTextColor = Color.Black,
            mainValueTextStyle = MaterialTheme.typography.displayLarge.copy(fontSize = 80.sp),
            percentSignColor = Color.Gray,
            percentSignTextStyle = MaterialTheme.typography.headlineSmall,
            budgetStatusIconColor = Color.Black,
            topIconResourceId = R.drawable.icon_power,
            topIconColor = Color.DarkGray
        )
    }
}