package com.example.testcomposeproject.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

@Immutable
data class WattsOnColors(
    val price_05: Color = Color(0xFFeb552b),
    val price_04: Color = Color(0xFFef8634),
    val price_03: Color = Color(0xFFf5bc50),
    val price_02: Color = Color(0xFFffd337),
    val price_01: Color = Color(0xFFfeea84),
    val primary_white: Color = Color(0xFFFFFFFF),
    val primary_white_off: Color = Color(0xFFfafafa),
    val primary_black: Color = Color(0xFF000000),
    val primary_black_off: Color = Color(0xFF2c2c2c),
    val primary_grey_05: Color = Color(0xFF7e7e7d),
    val primary_grey_04: Color = Color(0xFFa3a19f),
    val primary_grey_03: Color = Color(0xFFd7d5d1),
    val primary_grey_02: Color = Color(0xFFeae8e5),
    val primary_grey_01: Color = Color(0xFFf5f4f2),
    val primary_yellow: Color = Color(0xFFffd337),
    val primary_yellow_faded: Color = Color(0xFFfae181),
    val co2_05: Color = Color(0xFF523224),
    val co2_04: Color = Color(0xFFa98c4e),
    val co2_03: Color = Color(0xFFd3d968),
    val co2_02: Color = Color(0xFF80c362),
    val co2_01: Color = Color(0xFF59a46a),
    val kwh_05: Color = Color(0xFF283b3d),
    val kwh_04: Color = Color(0xFF3d6e71),
    val kwh_03: Color = Color(0xFF47989e),
    val kwh_02: Color = Color(0xFF8db0b3),
    val kwh_01: Color = Color(0xFFbacfd1),
    val system_blue_dark: Color = Color(0xFF085389),
    val system_malachite: Color = Color(0xFF24ce68),
    val system_orange: Color = Color(0xFFff9154),
    val system_blue: Color = Color(0xFF3faad9),
    val system_red_legacy: Color = Color(0xFFff5f54),
    val system_red_dark: Color = Color(0xFFb00000),
    val system_green_legacy: Color = Color(0xFF3bb352),
    val climate_05: Color = Color(0xFF25b800),
    val climate_04: Color = Color(0xFF82d31a),
    val climate_03: Color = Color(0xFFb6d865),
    val climate_02: Color = Color(0xFFcbe1b5),
    val climate_01: Color = Color(0xFFdfe4d7),
    val rank_01: Color = Color(0xFF59a46a),
    val rank_02: Color = Color(0xFF80c362),
    val rank_03: Color = Color(0xFFd3d968),
    val rank_04: Color = Color(0xFFffd337),
    val rank_05: Color = Color(0xFFeb552b),
)

private val DarkColorScheme = darkColorScheme(
    primary = Purple80,
    secondary = PurpleGrey80,
    tertiary = Pink80
)

private val LightColorScheme = lightColorScheme(
    primary = Purple40,
    secondary = PurpleGrey40,
    tertiary = Pink40
)

val LocalWattsOnColors = staticCompositionLocalOf {
    WattsOnColors()
}

@Composable
fun TestComposeProjectTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current

    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    val wattsOnColors = WattsOnColors()

    CompositionLocalProvider(
        LocalWattsOnColors provides wattsOnColors
    ) {
        MaterialTheme(
            colorScheme = colorScheme,
            typography = Typography,
            content = content
        )
    }
}

object WattsOnTheme {
    val colors: WattsOnColors
        @Composable
        get() = LocalWattsOnColors.current
}